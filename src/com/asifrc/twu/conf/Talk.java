package com.asifrc.twu.conf;

import java.util.ArrayList;

/**
 * Created by archoud on 3/25/14.
 */
public class Talk {
    private final String lengthText;
    private int length;
    private String title;

    public static ArrayList<Talk> generateTasksFromInput(String inputString) {
        ArrayList<Talk> talks = new ArrayList<Talk>();
        String talkStrings[] = inputString.split("\n");
        for (String talkText : talkStrings) {
            talks.add(new Talk(talkText));
        }
        return talks;
    }

    public Talk(String talkText) {
        this.title = talkText.split("( [0-9]+)|( lightning)")[0];
        this.lengthText = talkText.replace(this.title + " ", "").trim();
        this.length = (this.lengthText.equals("lightning"))
                ? 5
                : Integer.parseInt(this.lengthText.replace("min", ""));
    }

    public String getLengthText() {
        return lengthText;
    }

    public int getLength() {
        return length;
    }

    public String displayTitle() {
        return title;
    }
}
