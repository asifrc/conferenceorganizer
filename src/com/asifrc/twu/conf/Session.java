package com.asifrc.twu.conf;

import java.util.ArrayList;

/**
 * Created by archoud on 3/27/14.
 */
public class Session implements Comparable<Session>{
    public enum SessionType {
        MORNING ("Morning", 180),
        AFTERNOON ("Afternoon", 240),
        OTHER ("Other", 0);

        private String sessionName;

        private int sessionLength;
        SessionType(String sessionName, int sessionLength) {
            this.sessionName = sessionName;
            this.sessionLength = sessionLength;
        }

        public String sessionName() { return sessionName; }
        public int sessionLength() { return sessionLength; }
    }
    private final SessionType sessionType;

    private final int totalTime;
    private ArrayList<Talk> talks = new ArrayList<Talk>();

    public Session(SessionType sessionType) {

        this.sessionType = sessionType;
        this.totalTime = sessionType.sessionLength();
    }

    public Session(int totalTime) {
        this.sessionType = SessionType.OTHER;
        this.totalTime = totalTime;
    }

    public int getTimeRemaining() {
        int usedTime = 0;
        for (Talk talk : talks) {
            usedTime += talk.getLength();
        }
        return totalTime - usedTime;
    }

    public void addTalk(Talk talk) {
        talks.add(talk);
    }

    public boolean hasTalk(Talk talk) {
        return talks.contains(talk);
    }

    public String display() {
        String displayText = "";
        for (Talk talk : talks) {
            displayText += talk.displayTitle() + "\n";
        }
        return displayText;
    }

    @Override
    public int compareTo(Session o) {
        //Sorts descending
        return o.getTimeRemaining() - this.getTimeRemaining();
    }
}
