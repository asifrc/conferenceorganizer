package com.asifrc.twu.conf;

import java.util.ArrayList;
import java.util.Collections;

/**
 * Created by archoud on 4/1/14.
 */
public class Conference {

    private ArrayList<Session> sessions = new ArrayList<Session>();;
    private ArrayList<Track> tracks = new ArrayList<Track>();

    public void addSession(Session session) {
        sessions.add(session);
    }

    public boolean addTalk(Talk talk) {
        Collections.sort(sessions);
        Session session = sessions.get(0);

        boolean success;
        if (session.getTimeRemaining() >= talk.getLength()) {
            sessions.get(0).addTalk(talk);
            success = true;
        }
        else {
            success = false;
        }
        return success;
    }


    public String display() {
        String displayText = "";
        int trackNumber = 1;
        for (Track track : tracks) {
            displayText += track.display(trackNumber++) + "\n";
        }
        return displayText;
    }

    public void addTrack(Track track) {
        this.tracks.add(track);
        track.addSessionsTo(this);
    }
}
