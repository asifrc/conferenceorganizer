package com.asifrc.twu.conf;

/**
 * Created by archoud on 4/1/14.
 */
public class Track {
    private Session morningSession;
    private Session afternoonSession;

    public Track() {
        this.morningSession = new Session(Session.SessionType.MORNING);
        this.afternoonSession = new Session(Session.SessionType.AFTERNOON);
    }

    public String display(int trackNumber) {
        String displayText = "Track " + trackNumber + ":\n";

        displayText += morningSession.display();
        displayText += "12:00PM Lunch\n";
        displayText += afternoonSession.display();
        return displayText;
    }

    public void addSessionsTo(Conference conference) {
        conference.addSession(morningSession);
        conference.addSession(afternoonSession);
    }
}
