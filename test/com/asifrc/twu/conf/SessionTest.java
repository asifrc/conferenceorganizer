package com.asifrc.twu.conf;

import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

/**
 * Created by archoud on 3/26/14.
 */
public class SessionTest {
    @Test
    public void emptySessionShouldHave180MinutesFree() throws Exception {
        assertEquals(180, new Session(180).getTimeRemaining());
    }

    @Test
    public void shouldDecreaseRemainingTimeByTalkLengthWhenNewTalkAdded() throws Exception {
        Session session = new Session(Session.SessionType.MORNING);
        session.addTalk(new Talk("Lua for the Masses 30min\n"));
        assertEquals(150, session.getTimeRemaining());
    }

    @Test
    public void shouldVerifyIfTheSessionContainsASpecificTalk() throws Exception {
        Session session = new Session(Session.SessionType.AFTERNOON);
        Talk talk = new Talk("Rails Magic 60min\n");
        session.addTalk(talk);
        assertTrue(session.hasTalk(talk));
    }

    @Test
    public void shouldDisplayAllTalksInSession() throws Exception {
        //Needs to be changed when Talk time is displayed
        String expected =     "Overdoing it in Python\n"
                            + "Lua for the Masses\n"
                            + "Ruby Errors from Mismatched Gem Versions\n"
                            + "Common Ruby Errors\n";
        Session session = new Session(Session.SessionType.MORNING);
        session.addTalk(new Talk("Overdoing it in Python 45min\n"));
        session.addTalk(new Talk("Lua for the Masses 30min\n"));
        session.addTalk(new Talk("Ruby Errors from Mismatched Gem Versions 45min\n"));
        session.addTalk(new Talk("Common Ruby Errors 45min\n"));
        assertEquals(expected, session.display());
    }
}

