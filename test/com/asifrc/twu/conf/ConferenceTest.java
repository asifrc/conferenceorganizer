package com.asifrc.twu.conf;

import org.junit.Test;

import static org.junit.Assert.*;

/**
 * Created by archoud on 4/1/14.
 */
public class ConferenceTest {
    @Test
    public void shouldAddATalkToTheSessionWithMostSpace() throws Exception {
        Session sessionWithMediumTimeRemaining = new Session(100);
        Session sessionWithMostTimeRemaining = new Session(150);
        Session sessionWithLeastTimeRemaining = new Session(50);

        Conference conference = new Conference();

        conference.addSession(sessionWithMediumTimeRemaining);
        conference.addSession(sessionWithMostTimeRemaining);
        conference.addSession(sessionWithLeastTimeRemaining);

        Talk talk = new Talk("Ruby Errors from Mismatched Gem Versions 45min\n");
        conference.addTalk(talk);

        assertTrue(sessionWithMostTimeRemaining.hasTalk(talk));
    }

    @Test
    public void shouldNotAddATalkIfThereIsNotEnoughTimeRemainingInAnySessions() throws Exception {
        Session sessionWithMediumTimeRemaining = new Session(15);
        Session sessionWithMostTimeRemaining = new Session(35);
        Session sessionWithLeastTimeRemaining = new Session(40);

        Conference conference = new Conference();

        conference.addSession(sessionWithMediumTimeRemaining);
        conference.addSession(sessionWithMostTimeRemaining);
        conference.addSession(sessionWithLeastTimeRemaining);

        Talk talk = new Talk("Ruby Errors from Mismatched Gem Versions 45min\n");
        assertFalse(conference.addTalk(talk));
        assertFalse(sessionWithMostTimeRemaining.hasTalk(talk));
    }

    @Test
    public void shouldDisplayAllSessions() throws Exception {
        Conference conference = new Conference();

        conference.addTrack(new Track());

        conference.addTalk(new Talk("Lua for the Masses 100min\n"));
        conference.addTalk(new Talk("Ruby Errors from Mismatched Gem Versions 100min\n"));

        //Should include time once the concept of time exists
        String expected = "Track 1:\n"
                + "Ruby Errors from Mismatched Gem Versions\n"
                + "12:00PM Lunch\n"
                + "Lua for the Masses\n"
                + "\n";

        assertEquals(expected, conference.display());
    }

    @Test
    public void shouldDisplayOneLunchSlotWhenOneTrackIsAdded() throws Exception {
        Conference conference = new Conference();
        conference.addTrack(new Track());
        assertEquals("Track 1:\n12:00PM Lunch\n\n", conference.display());
    }
}
