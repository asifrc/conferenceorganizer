package com.asifrc.twu.conf;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

/**
 * Created by archoud on 4/1/14.
 */
public class TrackTest {
    @Test
    public void shouldDisplayOnlyATrackNameAndALunchSlot() throws Exception {
        Track track = new Track();
        assertEquals("Track 1:\n12:00PM Lunch\n", track.display(1));
    }
}
