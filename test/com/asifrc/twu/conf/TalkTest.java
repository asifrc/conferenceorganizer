package com.asifrc.twu.conf;

import org.junit.Test;

import java.util.ArrayList;

import static org.junit.Assert.assertEquals;

/**
 * Created by archoud on 3/25/14.
 */
public class TalkTest {
    static final String TEST_INPUT =  "Writing Fast Tests Against Enterprise Rails 60min\n" +
                        "Overdoing it in Python 45min\n" +
                        "Lua for the Masses 30min\n" +
                        "Ruby Errors from Mismatched Gem Versions 45min\n" +
                        "Common Ruby Errors 45min\n" +
                        "Rails for Python Developers lightning\n" +
                        "Communicating Over Distance 60min\n" +
                        "Accounting-Driven Development 45min\n" +
                        "Woah 30min\n" +
                        "Sit Down and Write 30min\n" +
                        "Pair Programming vs Noise 45min\n" +
                        "Rails Magic 60min\n" +
                        "Ruby on Rails: Why We Should Move On 60min\n" +
                        "Clojure Ate Scala (on my project) 45min\n" +
                        "Programming in the Boondocks of Seattle 30min\n" +
                        "Ruby vs. Clojure for Back-End Development 30min\n" +
                        "Ruby on Rails Legacy App Maintenance 60min\n" +
                        "A World Without HackerNews 30min\n" +
                        "User Interface CSS in Rails Apps 30min\n";
    @Test
    public void shouldCreateNewTalkObjectForEachLineInInputString() throws Exception {

        ArrayList<Talk> talks = Talk.generateTasksFromInput(TEST_INPUT);
        assertEquals(19, talks.size());
    }

    @Test
    public void shouldExtractTheCorrectLengthTextFromTheInputString() throws Exception {
        Talk talk = new Talk("Writing Fast Tests Against Enterprise Rails 60min\n");
        assertEquals("60min", talk.getLengthText());
    }

    @Test
    public void shouldExtractTheCorrectLengthTextFromTheInputStringOfALighteningTalk() throws Exception {
        Talk talk = new Talk("Rails for Python Developers lightning\n");
        assertEquals("lightning", talk.getLengthText());
    }

    @Test
    public void shouldCreateANumericalLengthFromTheInputString() throws Exception {
        Talk talk = new Talk("Overdoing it in Python 45min\n");
        assertEquals(45, talk.getLength());
    }

    @Test
    public void shouldCreateLengthOfFiveMinutesForALightningTalk() throws Exception {
        Talk talk = new Talk("Rails for Python Developers lightning\n");
        assertEquals(5, talk.getLength());
    }

    @Test
    public void shouldExtractTheTitleOfATalkFromTheInputString() throws Exception {
        Talk talk = new Talk("A World Without HackerNews 30min\n");
        assertEquals("A World Without HackerNews", talk.displayTitle());
    }
}
